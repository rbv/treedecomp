import igraph as ig
import numpy as np
import time

def bits_to_list(n):
    "Returns a list of the set bits in an integer"
    ret = []
    i = 0
    while n != 0:
        if n & (1 << i):
            ret.append(i)
            n -= 1 << i
        i += 1
    return ret

def bits_to_list2(n, maxlen):
    "Returns a list of the set bits in an integer"
    return [i for i in xrange(maxlen) if n & (1 << i)]

def bits_to_list3(n):
    "Returns a list of the set bits in an integer"
    ret = []
    i = 0
    while n != 0:
        if (n >> i) & 3 == 3:
            ret.append(i)
            ret.append(i+1)
            n -= 3 << i
            i += 2
        elif n & (1 << i):
            ret.append(i)
            n -= 1 << i
            i += 1
        elif n & (15 << i):
            i += 1
        else:
            i += 4
    return ret

def bitcount(n):
    ret = 0
    while n:
        n &= n - 1
        ret += 1
    return ret

class BEdgeMatrix(list):
    "Bits (Upper Triangular) Edge Matrix"
    def __init__(self,G,remapping=None):
        if isinstance(G, ig.Graph):
            #M = G.get_adjacency(ig.GET_ADJACENCY_UPPER)
            ret = [0] * G.vcount()
            #for i,row in enumerate(M):
            #    for j in range(len(row)):
            #        if row[j]:
            self.vertices = G.vcount()
            for e in G.es:
                e1,e2 = e.tuple #sorted(e.tuple)
                if remapping:
                    e1,e2 = remapping[e1], remapping[e2]
                if e1<e2:
                    ret[e1] |= 1 << e2
                else:
                    ret[e2] |= 1 << e1
            list.__init__(self,ret)
            self.check()
            #return ret

    def __contains__(self, edge):
        if edge[0] > edge[1]:
            edge = sorted(edge)
        return self[edge[0]] & (1 << edge[1])

    def __iadd__(self, edge):
        if edge[0] > edge[1]:
            edge = sorted(edge)
        self[edge[0]] |= 1 << edge[1]

    def __isub__(self, edge):
        if edge[0] > edge[1]:
            edge = sorted(edge)
            self[edge[0]] &= ~(1 << edge[1])

    def __str__(self):
        return str([bits_to_list(a) for a in self])

    def check(self):
        for v in range(len(self)):
            if self[v] & ((1 << v) - 1):
                print "bad row", v, "is", bin(self[v])

    def edges(self):
        return [ [(i,j) for j in bits_to_list(a)]
                 for i,a in enumerate(self) ]

    def higher_neighbours(self, v):
        return bits_to_list(self[v])

    def num_higher_neighbours(self, v):
        return bitcount(self[v])

    def make_clique_from_higher_neighbours(self, v):
        #print "make clique from neighbours", bits_to_list(self[v]), "of", v
        for n in bits_to_list2(self[v], self.vertices):
        #for n in bits_to_list(self[v]):
            #print "adding edges (", n,",", bits_to_list(self[v] & ~((2 << n) - 1)) 
            self[n] |= self[v] & ~((2 << n) - 1)

class EdgeMatrix(list):
    "(Upper Triangular) Edge Matrix"
    def __init__(self,G):
        if isinstance(G, ig.Graph):
            M = G.get_adjacency(ig.GET_ADJACENCY_UPPER)
            ret = np.zeros((G.vcount(),G.vcount()), dtype=np.int8)
            for i,row in enumerate(M):
                for j in range(len(row)):
                    if row[j]:
                        ret[i] |= 1 << j
            list.__init__(self,ret)

    def __contains__(self, edge):
        if edge[0] > edge[1]:
            edge = sorted(edge)
        return self[edge[0]] & (1 << edge[1])

    def __iadd__(self, edge):
        if edge[0] > edge[1]:
            edge = sorted(edge)
        self[edge[0]] |= 1 << edge[1]

    def __isub__(self, edge):
        if edge[0] > edge[1]:
            edge = sorted(edge)
        self[edge[0]] &= ~(1 << edge[1])

    def __str__(self):
        return str([bits_to_list(a) for a in self])

    def edges(self):
        return [ [(i,j) for j in bits_to_list(a)]
                 for i,a in enumerate(self) ]

def time_EM(G, num=10000, const = BEdgeMatrix):
    t1=time.clock()
    m = range(G.vcount())
    for i in xrange(num):
        M = const(G,m)
    t1 = time.clock() - t1
    print "called", const, num, "times in ", t1, "s"
