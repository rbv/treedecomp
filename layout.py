import igraph as ig
from vec2d import Vec2d

def line_off(p1, p2, p3):
    """Calculate the position of p3 relative to the projection of p3 onto the 
    (infinite) line containing p1, p2"""
#    p1 = Vec2d(p1)
#    p2 = Vec2d(p2)
#    p3 = Vec2d(p3)
    d = (p2 - p1).normalized()  # line vector
    d3 = p3 - p1
    proj3 = d.dot(d3) * d
#    print d3
#    print d3.normalized()
#    print proj3
    return d3 - proj3

def dist_from_line(p1, p2, p3):
    """Distance of p3 from line between p1, p2, as a vector!"""

#    p1 = Vec2d(p1)
    l_1 = (p2 - p1).normalized()  # line vector from p1
    d3_1 = p3 - p1  # p3 from p1
    if l_1.dot(d3_1) < 0:
        # p3 lies opposite from p2 around p1
        return d3_1  #d3_1.length
    
    #l_2 = -l_1   # line direction from p2
    d3_2 = p3 - p2  # p3 from p2
    if l_1.dot(d3_2) > 0:
        # p3 lies opposite from p1 around p2
        return d3_2  #.length

    # p3 lies between p1 and p2; return distance from line
    return d3_1 - l_1.dot(d3_1) * l_1

l_test = ig.Graph(n=3, edges=[(0,1)])
#ig.plot(l_test, bbox=(0,0,500,500), layout = ((-2,4), (7, 0), (2.8,2)))

def move_off_edge(point, edge, min_dist = 0.01, max_move = 0.01):
    """Should have max_move <= min_dist"""
    #edge is a pair of pairs
    off = dist_from_line(edge[0], edge[1], point)
    l = off.normalize_return_length()
    #print l
    if l < min_dist:
        #print "moving", tuple(point)
        if l == 0:
            move = (edge[1] - edge[0]).perpendicular_normal() * max_move
        else:
            move = min(max_move, min_dist - l) * off  #  ((dist / l) - 1) * off
        return move
    #return point

def tweak_layout(G, layout, dist=0.01, max_move=0.01):
    lay = [Vec2d(p) for p in layout]
    moves = 0
    for v3 in range(len(G.vs)):
        for v1, v2 in (edge.tuple for edge in G.es):
            if v1 == v3 or v2 == v3: continue
            move = move_off_edge(lay[v3], (lay[v1], lay[v2]), dist, max_move)
            if move:
                #lay[v3] += move
                move /= 3
                lay[v3] += move * 2
                lay[v1] -= move
                lay[v2] -= move
                moves += 1
        #layout[v3] = lay[v3]
    print "%d moves" % moves
    return ig.Layout(lay)

def test_layout(G, layout, unit = 0.01, verbose = True):
    lay = [Vec2d(p) for p in layout]
    bad = 0
    realbad = 0
    for v3 in range(len(G.vs)):
        m = min([(dist_from_line(lay[e[0]], lay[e[1]], lay[v3]).length, e)
                 for e in (edge.tuple for edge in G.es) if v3 not in e] + [(999999,0)])
        if verbose: print m
        if m[0] < unit * 2: bad += 1
        if m[0] < unit: realbad += 1
    #if verbose:
    print "bad:", bad, "realbad:", realbad
    #else:
    return bad, realbad
            
def iterate(G, layout, unit = None):
    if unit is None:
        bb = layout.bounding_box().coords
        unit = Vec2d(bb[2] - bb[0], bb[3] - bb[1]).length / 50   #
        #unit = ((bb[2] - bb[0]) * (bb[3] - bb[1]) / len(G.es)) ** 0.5 / 10   # by area
        print "Chose unit =", unit

    l = layout
    for it in range(20, 0, -1):
        bad, realbad = test_layout(G, l, unit, False)
        if realbad == 0: break
        #dist = 0.02 + it * 0.001
        dist = unit * (2 + it / 10.)
        #print dist
        l = tweak_layout(G, l, dist, unit)
    
    return l
