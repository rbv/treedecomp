import cairo

class _IGraphPlottableText(object):
    """Helper for igraph_plot_text"""

    def __plot__(self, context, bbox, palette, x, y, text, size, colour, xcentre, *args, **kwds):
        context.select_font_face("sans-serif", cairo.FONT_SLANT_NORMAL, \
                                     cairo.FONT_WEIGHT_BOLD)
        context.set_font_size(size)
        xb, yb, w, h = context.text_extents(text)[:4]
        if xcentre:
            x -= w/2.
        x-= xb
        #y -= h/2. + yb
        y -= yb
        context.move_to(x, y)
        context.set_source_rgb(*colour)
        context.text_path(text)
        context.fill()


def igraph_plot_text(plot, x, y, text, size = 14, colour = (0., 0., 0.)):
    """
    Draw some text on an igraph.Plot object
    """
    plot.add(_IGraphPlottableText(), None, None, 0.5, x, y, text, size, colour, True)

