import igraph as ig
import vorder as V
import sys

#fname = "404"
fname="Q8"
sys.stdout = open(fname + "_mindeg.txt", 'w')
#G = V.EGraph.From_DIMACS("graphs/" + fname + ".dgf")
G = V.Qn(8)
H, seq = V.random_mindeg_width_histogram (G,150000)
print H
#print seq
print list(H.bins())
ig.plot(H,fname + "_mindeg_hist.png")
