

def bits_to_list(n):
    "Returns a list of the set bits in an integer"
    ret = []
    i = 0
    while n != 0:
        if n & (1 << i):
            ret.append(i)
            n -= 1 << i
        i += 1
    return ret



def bitcount(n):
    ret = 0
    while n:
        n &= n - 1
        ret += 1
    return ret


def choose(n, k):
    if k > n - k: # take advantage of symmetry
        k = n - k
    c = 1
    for i in range(k):
        c = c * (n - i)
        c = c / (i + 1)
    return c

def choose2(n, k):
    from math import factorial
    return factorial(n) / (factorial(k) * factorial(n - k))

def Q(n):
    print "p edge",2**n , (2**n)*n/2
    for i in range(0, 2**n):
        for bit in  range(0, n):
            j = i | 2**bit
            if j != i and j < 2**n:
                print "e", (i+1), (j+1)


def Qn_pw(n):
    ret = 0
    for i in range(n):
        ret += choose(i, int(i/2))
    return ret

def Qn_tw_lowerbound(n):
    return int(3 * 2 ** n / (2 * n + 8)) - 1

def Qn_tw_Chl_upper(d):
    return choose(d, int((d-1)/2)) + choose(d, int((d+1)/2)) -1

def Qn_approx_b_v_lower(d):
    #usually overestimates, sometimes might underestimate?, this lower bound
    l = int(2**d / 4)
    #determine approx b_v(l, Q_d)
    ret = 0
    r = 0
    while l > 0:
        l -= choose(d, r)
        r += 1
    return choose(d, r) - 1

def Qn_my_guess_lower(d):
    return choose(d, int(d/2)) - 1

#Q(8)

def est1(n):
    from math import sqrt
    return 0.48 * 2**n / sqrt(n)

def est2(n):
    return float(choose(n, int(n/2)))

def simple_TD(d):
    "width of a simple alternating-layers-with-branches TD of Q_n"
    return choose(d, int(d/2)) + choose(d, int(d/2) - 2)

def known(n):
    if n > 4: return ""
    return str([1,2,4,6][n-1])

def h_upper(n):
    if n <= 4 or n > 8 : return ""
    return str([12,22,42,78][n-5])

def Szymanski_lower(n):
    return int((2**n - 1 - 1) / n)  + 1

def foo():
    for n in range(1, 41):
        print "Q%2d" % n, " vertices: %5d" % 2**n , "edges: %6d" % ((2**n)*n/2), "lower: %4d" % Qn_tw_lowerbound(n), "  approx lower: %5d" % Qn_approx_b_v_lower(n), " myguesslower: %4d" % Qn_my_guess_lower(n), " Szymanski_lower: %5d" % Szymanski_lower(n), " pw: %5d"% Qn_pw(n), " Chlebikova upper:  %5d" % Qn_tw_Chl_upper(n) 
#        print "$Q_{%d}$" % n, " & %4d" % 2**n , " & %5d" % ((2**n)*n/2), " & %4d" % Qn_approx_b_v_lower(n), " & %4d" % Qn_my_guess_lower(n), " & ", known(n), " & ", h_upper(n), " & ", Qn_pw(n)

def bar():
    for i in range(2,20):
        print i, choose(i, int(i/2)), 2*choose(i, int(i/2)-1)

def bound_ratio(d, r):
    return choose(d, r+1) *1.0 / sum([choose(d,i) for i in range(0,r+1)])

def ratios(d):
    return [bound_ratio(d,r) for r in range(0,int(d/2+1.5))]
