#!/usr/bin/env python

import sys
import time
import vorder
import optparse


def mindeg_width(G, num=1000, force_init=[]):
    start_t = time.clock()
    best = [[], G.vcount()]
    for i in xrange(num):
        seq,ans1 = random_mindegree_TW(G, force_init)
        if ans1 < best[1]:
            best = seq, ans1
        ret.add(ans1)
    print "random_mindeg_width_histogram:", num, "seqs evaluated in", time.clock() - start_t, "s"
    print "best sequence (width %d):" % best[1]
    print str(best[0])
    return ret, best[0]


parser = optparse.OptionParser(usage="%prog [options] graph_file", description="Compute tree- or path-decomposition of a graph heuristically. Outputs result to stdout, info to stderr.")
parser.add_option("-p", dest="path", action="store_true", default=False,
          help="Find a path decomposition instead of tree decomposition")
parser.add_option("-q", dest="quiet", action="store_true", default=False,
          help="Write nothing to stderr")
parser.add_option("-w", dest="width_only", action="store_true", default=False,
          help="Produce width rather than decomposition")
parser.add_option("-v", dest="vacs", action="store_true", default=False,
          help="Produce VACS textual t-parse format")
parser.add_option("-r", dest="renumber", action="store_false", default=True,
          help="For debugging, don't renumber vertices into range 0..width")
parser.add_option("-c", dest="components", action="store_true", default=False,
          help="Produce a separate t-parse per component")

(options, args) = parser.parse_args()

if len(args) != 1:
    parser.print_help()
    exit("\nExpected single input file.\n")

if options.quiet:
    def silence(*args): pass
    cerr = silence
else:
    cerr = sys.stderr.write

infile = args[0]
if infile.endswith(".dgf"):
    cerr("Assuming DIMACs file format\n")
    graph = vorder.EGraph.From_DIMACS(infile)
else:
    cerr("Assuming adjacency lists file format\n")
    graph = vorder.EGraph.From_Adjlists(infile)

print graph.vcount()

TD = 3

if options.width_only:
    print TD.width()
else:
    print TD.tparse(graph, options.vacs, options.renumber)
