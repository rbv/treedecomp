#!/usr/bin/env python

import sys
import vorder as V

title = ""
filen = None

if len(sys.argv) > 1:
    if sys.argv[1] == '-save' or sys.argv[1] == '--save':
        filen = sys.argv[2]
    else:
        assert False

#for line in sys.stdin.xreadlines():
g_num = 0
while 1:
    line = sys.stdin.readline()
    if line == '': break
    line = line.strip()
    if len(line) > 2:
        pieces = line.split()
        for piece in pieces:
            #if line.startswith("Graph "):
            #    piece = line[6:]
            if piece[0] == '[' and piece[-1] == ']':
                G, TD, boundary = V.parse_tparse(piece[1:-1])
                G.label_boundary(boundary)
                args = {}
                if filen:
                    args['save_to'] = "%s%04d.png" % (filen, g_num)
                    g_num += 1

                G.plot(title=title, subtitle=line, **args)
                break
        else:
            title = line
