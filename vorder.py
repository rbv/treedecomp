import igraph as ig
import numpy as np
from itertools import combinations, permutations
import random
import layout as custom_layout
#from IPython.background_jobs import BackgroundJobManager as Jobs
try:
    import IPython
except ImportError:
    pass
import cairo
from drawing import igraph_plot_text
import time
import copy
from ematrix import BEdgeMatrix, EdgeMatrix, bits_to_list, bitcount
from Qn import choose

#jobs = IPython.__builtins__['jobs']


ig.random = ig.Graph.Erdos_Renyi


def plot2(g):
    ig.plot(g, layout='fr', margin=70, bbox=(500,500))


ig.plot2 = plot2


#def lowest_bit(n):
#    "Returns the lowest set bit in n"
#    return n - (n & (n - 1))


def label(g1, labelling = str):
    for v in g1.vs:
        v['label'] = labelling(v.index)

def disjoint_union(g1, g2, default = ''):
    ""
    ret = g1 + g2

    v1_attr = set(g1.vs.attribute_names())
    v2_attr = set(g2.vs.attribute_names())
    e1_attr = set(g1.es.attribute_names())
    e2_attr = set(g2.es.attribute_names())

    for attr in v1_attr.union(v2_attr):
        if attr in v1_attr:
            val = g1.vs.get_attribute_values(attr)
        else:
            val = [default] * g1.vcount()
        if attr in v2_attr:
            val += g2.vs.get_attribute_values(attr)
        else:
            val += [default] * g1.vcount()
        ret.vs[attr] = val
        
    return ret


def layout_Qn(n):
    ret = [None] * (2**n)
    wide = 1./(2+choose(n, int(n/2)))
    for L in range(0, n+1):
        num = choose(n, L)
        y = 1.0/(n+2) * (L + 1)
        x = (1. - (num + 2) * wide) / 2
        for i in range(0, 2**n):
            if bitcount(i) == L:
                #print "i=", i , "in", L, y
                ret[i] = (x, y)
                x += wide
    return ret
                

#g = ig.random(n = 17, m = 38)

#g.write(filename + '.graph', format='pickle')
#g = ig.read(filename + 'graph', format='pickle')

#ig.plot(g, layout='fr') 

# Enhanced Graph
class EGraph(ig.Graph):

    # def __new__(cls, *args, **kwargs):
    #     if len(args) == 1:
    #         if isinstance(args[0], ig.Graph):
    #             return args[0]

    @staticmethod
    def From_Matrix(M):
        ret = EGraph(len(M))
        M.check()
        for v in range(len(M)):
            for n in M.higher_neighbours(v):
                ret += (v,n)
        return ret

    #random = ig.Graph.Erdos_Renyi
    @staticmethod
    def Random(*args, **kwargs):
        return EGraph.Erdos_Renyi(*args, **kwargs)

    @staticmethod
    def From_DIMACS(filename):
        f = open(filename, 'rU')
        #ret = None
        for line in f.readlines():
            if line[0:1] == "c":
                pass
            elif line[0:1] == "p":
                dummy, dummy, vcount, ecount = line.split()
                ret = EGraph(int(vcount))
            elif line[0:1] == "e":
                edges = line.split()[1:3]
                edges = (int(edges[0]) - 1, int(edges[1]) - 1)
                #print repr(edges)
                ret += edges
            else:
                print "READ ERROR"
        f.close()
        return ret

    @staticmethod
    def From_Adjlists(filename):
        with open(filename, 'rU') as f:
            vertices = int(f.readline())
            ret = EGraph(vertices)
            lines = f.readlines()
            if len(lines) != vertices:
                raise ValueError("Expected %d lines of adjacency lists, but found %d" % (vertices, len(lines)))
            for vertex, line in enumerate(lines):
                ret += [(vertex, int(n)) for n in line.split()]
            return ret

    def subgraph_edges(self, edges):
        "Creates subgraph containing a subset of edges"
        ret = self.copy()
        if isinstance(edges[0], ig.Edge):
            edges = [e.index for e in edges]
        def complement(e):
            return e.index not in edges
        ret -= self.es(complement)   # it works! igraph looks only at the edge index
        return ret

    def boundary_join(self, boundary, other, other_boundary):
        """
        Join another graph to this one along their boundaries (vertex sequences).

        Returns (result, permutation) where permutation is the mapping from other's vertices to the result's.
        self's vertices map unchanged.
        """
        if len(boundary) != len(other_boundary):
            raise ValueError("boundaries have different sizes")
        G1_vcount = self.vcount()
        G2_vcount = other.vcount()
        ret = self.copy()
        ret += G2_vcount - len(boundary)
        other = other.copy()
        other += G1_vcount - len(boundary)
        permute = [None] * G2_vcount #+ range(G1_vcount - len(boundary))
        new_v_id = G1_vcount
        for i in range(G2_vcount):
            if i in other_boundary:
                permute[i] = boundary[other_boundary.index(i)]
            else:
                permute[i] = new_v_id
                new_v_id += 1
        #print "permutation:", permute
        other = other.permute_vertices(permute + list(set(range(ret.vcount())).difference(permute)))
        return ret.union(other), permute  # not inplace

        #ret = EGraph(self.vcount() + other.vcount() - len(boundary))

    def is_TD_for(self, graph):
        if 'bag' not in self.vs.attribute_names():
            print "I don't have bags"
            return False
        failed = False

        # Check all edges covered
        M = graph.get_adjacency(ig.GET_ADJACENCY_UPPER)
        for bag in self.vs['bag']:
            sortedbag = sorted(list(bag))
            for i,j in combinations(sortedbag,2):
                    M[i, j] = 0
        for i,row in enumerate(M):
            if any(row):
                failed = True
                print "fail!"
                for j in range(len(row)):
                    if row[j]:
                        print "edge %d,%d missing" % (i, j)
        if failed:
            print M

        # Check bag preimages are connected and nonempty
        for v in range(graph.vcount()):
            def in_preimage(vertex):
                return v in vertex['bag']
            subtree = self.subgraph(self.vs.select(in_preimage))
            if subtree.vcount() == 0:
                failed = True
                print "vertex", v, "is in no bag"
            elif not subtree.is_connected():
                failed = True
                print "vertex", v, "has disconnected preimage"
                
        return not failed

    def hilite_vertices(self, vertices):
        for v in self.vs: v['color'] = 'white'
        for v in self.vs[vertices]: v['color'] = 'red'

    def hilite_edges(self, edges):
        for e in self.es: e['color'] = (180, 180, 255)
        for e in self.es[edges]: e['color'] = 'black'

    def hilite_closure(self, vertices):
        "Hightlights a set of vertices, and all the edges in their induced subgraph"
        self.hilite_vertices(vertices)
        for e in self.es:
            if e.source in vertices and e.target in vertices:
                e['color'] = 'black'
            else:
                e['color'] = (180, 180, 255)

    def label_boundary(self, mapping):
        "Using a mapping returned from parse_tparse, hilite & label boundary vertices"
        boundary = mapping.values()
        self.hilite_vertices(boundary)
        for v in self.vs:
            if v.index in boundary:
                v['label'] = str(boundary.index(v.index))
            else:
                v['label'] = ''


    def plot(self, **kwargs):
        """
        Lays out, renders, and displays the graph. Returns an igraph.Plot object

        Displays asynchronously using IPython JobsManager if possible.
        Add nw=1 to return the object without displaying it.
        Add layout="foo" (eg. graphopt) to override the layout.
        Add a title with the title keyword argument, subtitle with subtitle
        """
        if hasattr(self, 'label'):
            self.label()
        if not 'size' in self.vs.attribute_names():
            for v in self.vs: v['size'] = 8
        if 'layout' in kwargs:
            layout = kwargs['layout']
        elif self.ecount() < 50:
            layout = self.layout('fr')
            # Increase node repulsive force to 10x default
            # layout = self.layout('graphopt', node_charge = 0.01)
            # print "layout graphopt"

            # layout = self.layout('kk', initemp = 15, maxiter = 2000, coolexp = 0.994)
            # print "layout slow kk"
        else:
            layout = self.layout('fr')
        # Move any vertices that are lying on top of edges
        layout = custom_layout.iterate(self, layout)

        bbox = kwargs.get('bbox', ig.BoundingBox(500, 500))
        kwargs['bbox'] = bbox
        async = False
        if 'nw' not in kwargs:
            try:
                jobs = IPython.__builtins__['jobs']
                kwargs['nw'] = 1
                async = True
            except (NameError, KeyError):
                pass
        #if 'nw' in kwargs:
        kwargs['target'] = cairo.ImageSurface(cairo.FORMAT_ARGB32, int(bbox.width), int(bbox.height))
        if 'margin' not in kwargs:
            kwargs['margin'] = 70
        target = ig.plot(self, layout=layout, vertex_label_dist=1.4, **kwargs)
        #if 'nw' in kwargs:
        #draw immediately instead of waiting for target.show()
        target._bgcolor = (1., 1., 1.)
        target.redraw()
        if 'title' in kwargs:
            igraph_plot_text(target, bbox.width * 0.5, bbox.height * 0.01, kwargs['title'], 15)
        if 'subtitle' in kwargs:
            igraph_plot_text(target, bbox.width * 0.5, bbox.height * 0.05, kwargs['subtitle'], 11)

        if async:
            jobs.new(target.show)
        elif 'save_to' in kwargs:
            target.save(kwargs['save_to'])
        else:
            target.show()
        return target


class PreTreeDecomp(EGraph):

    def __init__(self, *args, **kwargs):
        super(PreTreeDecomp, self).__init__(*args, **kwargs)
        self.init(self)

    @staticmethod
    def init(self):
        self.have_edge_joins = False
        self.check_is_TD()
        self.number_nodes = False

    def check_is_TD(self):
        "Check whether this is a TD, updating self.is_TD. Assumes self is simple undirected!"
        self.comps = len(self.components())
        #if self.comps > 1:
        #    self.is_TD = False
        #    return False
        temp = self.vcount() - (self.comps + self.ecount())
        #if temp < 0:
        #    raise Exception("Too many edges!")
        self.is_TD = (temp == 0)
        #print "temp =", temp
        return self.is_TD

    @staticmethod
    def from_graph(g):
        "Equivalent to .linegraph, but sets 'bag' attributes"
        ret = PreTreeDecomp(g.ecount())

        adjs = g.get_adjedgelist()

        #for e in range(g.ecount()):
        for e in g.es:
            ret.vs[e.index]['bag'] = set(e.tuple)

            for incident_v in e.tuple:
                for adjedge in adjs[incident_v]:
                    if e.index < adjedge:
                        ret += (e.index, adjedge)
                        #ret.vs[e.index]['label'] += 
        return ret

    def label(self):
        for i,v in enumerate(self.vs):
            if self.number_nodes:
                v['label'] = str(i) + ": " + str(sorted(tuple(v['bag'])))
            else:
                v['label'] = str(sorted(tuple(v['bag'])))
            if 'label2' in self.vertex_attributes():
                if isinstance(v['label2'], str):
                    v['label'] += v['label2']

    def width(self):
        return max(len(v['bag']) for v in self.vs) - 1

    def process_edges(self):
        """For each edge, add 'join' attribute := intersection of endpoint bags,
        and 'weight' attribute := len('join')"""
        self.have_edge_joins = True
        for e in self.es:
            v1,v2 = e.tuple
            e['join'] = self.vs[v1]['bag'].intersection(self.vs[v2]['bag'])
            e['weight'] = len(e['join'])

    def path_edges(self, path):
        "Given a path as a list of vertex ids, return a list of edge ids."
        return [self.es[self.get_eid(v1,v2)] for v1,v2 in zip(path[0:-1], path[1:])]

    def cut_edge(self, e):
        "e is an edge"

        v1,v2 = e  #[self.vs[i] for i in e]
        common = self.vs[v1]['bag'].intersection(self.vs[v2]['bag'])
        #print "common is", common
        self -= e
        path = self.get_shortest_paths(v1)[v2]
        if len(path) == 0:
            self.comps += 1
            return
        #print "path is", path
        for v in path[1:-1]:
            self.vs[v]['bag'].update(common)
        if self.have_edge_joins:
            for edge in self.path_edges(path):
                edge['join'].update(common)
                edge['weight'] = len(edge['join'])

    def contract_edge(self, e, mapping = None):
        "Returns index of the new vertex"
        v1, v2 = sorted(e)  # so that v1's numbering does not change
        b1 = self.vs[v1]['bag']
        b2 = self.vs[v2]['bag']
        b1.update(b2)
        self.vs[v1]['bagbits'] |= self.vs[v2]['bagbits']
        new_edges = [(v1, v) for v in self.neighbors(v2) if (v1,v) not in self.es and v1 != v]
        self += new_edges
        # as long as this is a real TD, could update edge info in following way:
        # just preserve existing.
        # if self.is_TD:
        #     new_edges = [self.get_eid(*e) for e in new_edges]
        # else:
        self.have_edge_joins = False
        self -= v2
        if mapping is not None:
            #mapping[v2+1:] -= 1   #WARNING: assumes mapping is strictly increasing; really ought to find a in mapping st. a>>v2 instead
            #mapping[v2] = -1

            #mapping[mapping == v2] = -1
            #mapping[mapping > v2] -= 1

            mapping[mapping == v2] = v1
            mapping[mapping > v2] -= 1
        return v1

    def make_TD(self):
        while 1:
            if self.check_is_TD():
                break
            if not self.have_edge_joins:
                print "processing"
                self.process_edges()
            loop = self.girth(True)   #FIXME: so lazy
            if len(loop) < 2:
                # should have been caught above
                raise Exception()
            #m = min([(e['weight'], e) for e in self.es])
            m = min([(e['weight'], e) for e in self.path_edges(loop)])
            print "cutting", e, e.tuple
            self.cut_edge(m[1].tuple)
        width = self.width()
        print "Tree width =", width
        #self.reduce_TD()
        if self.width() != width:
            raise Exception()

    def safe_edge_iter(self, mapping):
        """Iterate over the edges in a way safe when edges are removed (as they are iterated over),
        and vertices are identified (ie. edges are moved).
        mapping provides a mapping from the original vertex numbering to the current one."""
        adjlists = self.get_adjlist()
        for i, adj in enumerate(adjlists):
            for v in adj:
                # if mapping[i] < 0: break  this vertex was deleted!
                # if mapping[v] < 0: continue  deleted
                # if mapping[i] < mapping[v]:
                #     yield (int(mapping[i]), int(mapping[v]))
                if mapping[i] < mapping[v]:
                    yield (int(mapping[i]), int(mapping[v]))
            
    def reduce_TD(self):
        "Turns this TD into a small TD"
        mapping = np.array(range(self.vcount()))
        #print "reducing"
        self.vs['bag'] = [set(b) for b in self.vs['bag']]
        for v1,v2 in self.safe_edge_iter(mapping):
            vv1 = self.vs[v1]
            vv2 = self.vs[v2]
            b1 = self.vs[v1]['bag']
            b2 = self.vs[v2]['bag']
            #print v1,v2,b1,b2
            if b1.issubset(b2) or b1.issuperset(b2):
            #if b1&b2 == b1 or b1&b2 == b2:
                # merge
                self.contract_edge((v1,v2), mapping)
            
        # for e in self.es:
        #     b1 = self.vs[e.source]['bag']
        #     b2 = self.vs[e.target]['bag']
        #     if b1.issubset(b2):
        #         b3 = b2
        #     elif b1.issuperset(b2):
        #         b3 = b1
        #     if b3:
        #         # merge
        #         self.contract_edge(e, mapping)
                
                


    def _tparse(self, graph, vacs_format = False, renumber = True):
        if vacs_format:
            formstr = "%x"
        else:
            formstr = "%d"

        M = graph.get_adjacency(ig.GET_ADJACENCY_UPPER)

        def recurse(node, parent, mapping):
            "Recurse down to leaf nodes of the TD. Returns (string, bag)"

            bag = self.vs[node]['bag']
            #print "TD node", node
            #print bag
            #print "initial mapping", mapping

            # mapping maps boundary vertex numbers to original vertex numbers or None if unused
            # (not used if not renumbering)
            if mapping is None:
                mapping = [None] * (width + 1)
            else:
                # clean unused vertices from mapping
                for i, v in enumerate(mapping):
                    if v not in bag:
                        mapping[i] = None

            #print "part mapping", mapping

            inv_mapping = {}
            for i, v in enumerate(mapping):
                if v is not None:
                    inv_mapping[v] = i

            # Add new vertices to mapping
            for vertex in bag:
                if vertex in inv_mapping:
                    continue
                for i, v in enumerate(mapping):
                    if v is None:
                        mapping[i] = vertex
                        inv_mapping[vertex] = i
                        break
                else:
                    raise Exception("tparse() bug!")

            #print "new mapping", mapping

            neighbors = set(self.neighbors(node))
            neighbors.discard(parent)
            if len(neighbors) == 0:
                # Prefix with node operators
                if renumber:
                    if vacs_format:
                        ret = " ".join((formstr % i) for i in range(width + 1))
                    else:
                        ret = " ".join((formstr % i) for i,v in enumerate(mapping) if v is not None)

                else:
                    ret = " ".join((formstr % i) for i in bag)
                   
            else:
                for i, n in enumerate(neighbors):
                    childret, childbag = recurse(n, node, list(mapping))
                    # New node vertices
                    for v in sorted(list(set(bag).difference(childbag))):
                        if renumber:
                            v = inv_mapping[v]
                        childret += " " + formstr % v
                    if i == 0:
                        ret = childret
                    else:
                        ret += " + (" + childret + ")"

            if parent is not None:
                parentbag = self.vs[parent]['bag']

            # add edge operators
            for i, j in combinations(sorted(list(bag)), 2):
                if not M[i, j]:
                    continue
                if parent is not None:
                    # Ignore edges witnessable by parent
                    if i in parentbag and j in parentbag:
                        continue
                if renumber:
                    i = inv_mapping[i]
                    j = inv_mapping[j]
                    if i > j:
                        i, j = j, i
                if vacs_format:
                    ret += " %x%x" % (j, i)
                else:
                    ret += " %d,%d" % (i, j)
                
            return ret, bag

        components = []

        for c in self.components():
            # Pick a root
            root = max([(self.degree(i), i) for i in c])
            root = root[1]

            width = self.subgraph(c).width()

            ret, bag = recurse(root, None, None)
            components.append(ret)

        return " \n".join(components)
            
    def tparse(self, graph, vacs_format = False, renumber = True, percomponent = True):
        """
        A textual t-parse of a graph, given this tree decomposition.

        vacs_format: Use VACS hexidecimal format
        renumber: Renumber vertices into range 0..width (not a proper t-parse without this)
        percomponent: Separate t-parse (on separate lines) per connected component
        """

        width = self.width()
        if width >= 15 and vacs_format:
            raise ValueError("VACS hexidecimal format only supports up to width 15; tree decomposition has width %d" % width)

        components = list(self.components())
        if len(components) > 1 and not percomponent:
            # just join everything together
            extra_edges = [(components[0][0], comp[0]) for comp in components[1:]]
            self += extra_edges
            ret = self._tparse(graph, vacs_format, renumber)
            self -= extra_edges
            return ret
        return self._tparse(graph, vacs_format, renumber)


class TreeDecomp(PreTreeDecomp):
    """A Tree Decomposition is a forest, where each vertex's 'bag' attribute
    is a list of vertices of the underlying graph"""

    pass

def parse_tparse(tparse):
    """Parse a textual t-parse, returning (graph, TD, boundary mapping)."""

    if ' ' in tparse and ',' in tparse:
        def parse_op(op):
            if ',' in op:
                op = op.split(',')
                return (int(op[0]), int(op[1]))
            return int(op)
    else:
         # VACS format, either space or comma separated
        tparse = tparse.replace(',', ' ')
        def parse_op(op):
            i = int(op, 16)
            #if i >= 16:
            if len(op) == 2:
                return (i % 16, i / 16)
            return i

    def recurse():
        G = EGraph(0)
        TD = TreeDecomp(1)
        node = TD.vs[0]  # current node in TD
        node['bag'] = set()
        mapping = {}  # maps boundary vertex numbers to vertices of G
        print "\nparsing"
        for op in tparse:
            print op,
            if op == ')':
                break
            if op == '(':
                raise ValueError("Unexpected (")
            if op == '+':
                op = next(tparse)
                if op != '(':
                    raise ValueError("Syntax error: should have (...) after +")
                G2, TD2, node2, mapping2 = recurse()

                # Find common boundary
                boundary = []
                boundary2 = []
                for i, v in mapping.iteritems():
                    if i in mapping2:
                        boundary.append(v)
                        boundary2.append(mapping2[i])
                print "mappings:", mapping, mapping2

                Gnew, G2_permutation = G.boundary_join(boundary, G2, boundary2)

                # join tree decompositions
                for v2 in TD2.vs:
                    v2['bag'] = set(G2_permutation[v] for v in v2['bag'])
                print "permutation:", G2_permutation
                print "new bags:", TD2.vs['bag']
                TDnew, TD2_permutation = TD.boundary_join([node.index], TD2, [node2.index])
                TDnew.init(TDnew)
                # add bags, since igraph throws away attributes when doing a union
                for v in TD2.vs:
                    TDnew.vs[TD2_permutation[v.index]]['bag'] = v['bag']
                for v in TD.vs:
                    # bags should be equal on the overlapping boundary... but sometimes one's a subset of the other
                    vnew = TDnew.vs[v.index]
                    b1 = vnew['bag']
                    b2 = v['bag']
                    if isinstance(b1, set) and b1 != b2 and not b1.issubset(b2) and not b1.issuperset(b2):
                        print v.index, node.index, node2.index, vnew['bag'], v['bag']
                        G.plot()
                        TD.plot()
                        time.sleep(0.3)
                        G2.plot()
                        TD2.plot()
                        raise Exception("")
                    vnew['bag'] = v['bag']
                G = Gnew
                TD = TDnew

                # mapping still the same
                continue

            op = parse_op(op)
            if isinstance(op, int):
                if op in mapping:  # mapping[op] != None:
                    # new bag
                    TD += 1
                    oldnode = node
                    node = TD.vs[TD.vcount() - 1]
                    node['bag'] = oldnode['bag'].difference([mapping[op]])
                    TD += (oldnode.index, node.index)
                # new vertex
                G += 1
                v = G.vcount() - 1
                mapping[op] = v
                node['bag'].add(v)
            else:
                G += (mapping[op[0]], mapping[op[1]])
        print TD.vs['bag']
        return G, TD, node, mapping

    tparse = iter(tparse.replace('(', ' ( ').replace(')', ' ) ').split())
    G, TD, _, mapping = recurse()
    if next(tparse, None) != None:
        raise ValueError("Unmatched ) in input")
    if not TD.check_is_TD():
        raise Exception("parser bug")
    if not TD.is_TD_for(G):
        raise Exception("parser bug")
    return G, TD, mapping #mapping.values()
    


def is_perfect_elim_ordering(G, seq):
    G = G.simplify()
    for vnum,v in enumerate(seq):
        n = [u for u in G.neighbors(v) if seq.index(u) > vnum]
        for i,j in combinations(n,2):
            if j not in G.neighbors(i):
                print "missing link between", i,j," -- higher neighbors of",v
                return False
    return True

def inverse_perm(seq):
    ret = [0] * len(seq)
    for i,v in enumerate(seq):
        ret[v] = i
    return ret

def TW_from_seq(G_or_adjlist, seq):
    """Given a undirected (simple) graph and an elimination order, returns the width of the resulting tree decomposition"""
    inverse = inverse_perm(seq)
    width = -1
    if isinstance(G_or_adjlist, list):
        adj = copy.deepcopy(G_or_adjlist)
    else:
        adj = [set(a) for a in G_or_adjlist.get_adjlist()]
    for vnum,v in enumerate(seq):
        higher_n = [i for i in adj[v] if inverse[i] > vnum]
        #print "clique from neighbours", higher_n, "of", v
        width = max(width, len(higher_n))
        #bagbits = sum(1 << v for v in higher_n)
        # Make a clique out of v's higher neighbourhood
        for v1,v2 in combinations(higher_n, 2):
            adj[v1].add(v2)
            adj[v2].add(v1)
    return width

def fast_TW_from_seq(G, seq):
    """Given a undirected (simple) graph and a VertexSeq, returns a tree decomposition (TD)"""
    inverse = inverse_perm(seq)
    M = BEdgeMatrix(G,inverse)
    #M.check()
    for v in range(len(seq)):
        M.make_clique_from_higher_neighbours(v)
    #return EGraph.From_Matrix(M)
    return max(bitcount(b) for b in M)


def TD_from_seq(G, seq):
    """Given a undirected (simple) graph and a VertexSeq, returns a tree decomposition (TD)"""
    H = G.copy().simplify()
    #M = BEdgeMatrix(H)
    TD = PreTreeDecomp(G.vcount())
    inverse = inverse_perm(seq)
    if sorted(inverse) != range(len(seq)):
        print "BAD SEQ"
        print repr(seq)
        print repr(inverse)
        return
    #print "seq:", seq
    #print "inverse seq: ", inverse

    # bitvector of the v's in seq already processed
    higher = 0

    for vnum,v in enumerate(seq):
        if vnum != inverse[v]:
            print "BAD INV"
        higher_n = [i for i in H.neighbors(v) if inverse[i] > vnum]
        
        v_t = TD.vs[v] # new tree vertex

        bagbits = sum(1 << v for v in higher_n)
        #bagbits &= higher
        #higher |= 1 << v
 
        # Make a clique out of v's higher neighbourhood
        #higher_n = bits_to_list(bagbits)  #higher numbered neighbours
        #print v, " : ", H.neighbors(v) , " higher_n : ", higher_n
        for i, v1 in enumerate(higher_n):
            for v2 in higher_n[i+1:]:
                if v1 not in H.neighbors(v2):
                    H += (v1,v2)

        v_t['bagbits'] = bagbits + (1 << v)
        v_t['bag'] = higher_n + [v]

        #higher_n_g =  [i for i in H.neighbors(v) if inverse[i] > inverse[v]]
        if len(higher_n):
            #could skip this if higher_n is built by order in seq
            first_neighbour_in_seq = min((inverse[u],u) for u in higher_n)[1]
            TD += (v, first_neighbour_in_seq)
        else:
            #DEBUG
            #if this isn't the first
            if vnum != len(seq) - 1:
                v_t['label2'] = " leaf"
                #print "leaf"
            #    #then attach arbitarily
            #    print "arb edge:", v, seq[vnum + 1]
            #    TD += (v, seq[vnum + 1])

    if not is_perfect_elim_ordering(H, seq):
        print "ERROR!===============Not a perfect elim ordering"
    if not TD.is_TD_for(G):
        print "ERROR!===============Not a TD"
   
    #print [(a,bits_to_list(b)) for a,b in unjoined]
    return TD, H


def sorted_TW(G,seq):
    return TW_from_seq (G.permute_vertices(inverse_perm(seq)), range(G.vcount()))

def Qn(n):
    ret=EGraph(2**n)
    for i in range(0, 2**n):
        for bit in  range(0, n):
            j = i | 2**bit
            if j != i and j < 2**n:
                ret+=[(i,j)]
    return ret

def common_Qn_show(n,seq,hilite=[],hilite2=[], **kwargs):
    G = Qn(n)
    TD, H = TD_from_seq(G, seq)
    G.hilite_closure(hilite + hilite2)
    for v in G.vs[hilite2]: v['color'] = 'purple'
    G_plot = G.plot(nw=1, layout=layout_Qn(n),**kwargs)
    
    TD.number_nodes = False
    TD.plot()
    TD.reduce_TD()
    TD.plot(margin=120, bbox=ig.BoundingBox(800,700))
    return TD, seq

Q7_bigbags=[[0, 3, 5, 6, 12, 17, 18, 20, 24, 33, 34, 39, 40, 43, 45, 46, 51, 53, 54, 57, 58, 60, 65, 66, 71, 72, 75, 77, 78, 83, 85, 86, 89, 90, 92, 105, 106, 111, 119, 123, 125, 126, 10], [0, 3, 5, 6, 10, 12, 13, 17, 18, 20, 24, 25, 33, 39, 40, 43, 45, 46, 51, 53, 54, 57, 58, 60, 65, 71, 72, 75, 77, 78, 83, 85, 86, 89, 90, 92, 105, 111, 119, 123, 125, 126, 11], [0, 6, 17, 18, 20, 24, 33, 34, 36, 39, 40, 46, 51, 53, 54, 57, 58, 60, 65, 66, 68, 71, 72, 78, 83, 85, 86, 89, 90, 92, 98, 99, 101, 105, 106, 108, 111, 118, 119, 123, 125, 126, 100], [0, 5, 6, 12, 17, 18, 20, 24, 33, 34, 36, 39, 40, 45, 46, 51, 53, 54, 57, 58, 60, 65, 66, 68, 71, 72, 77, 78, 83, 85, 86, 89, 90, 92, 99, 105, 106, 111, 119, 123, 125, 126, 101], [0, 6, 12, 17, 18, 20, 24, 33, 34, 36, 39, 40, 45, 46, 51, 53, 54, 57, 58, 60, 65, 66, 68, 71, 72, 77, 78, 83, 85, 86, 89, 90, 92, 99, 101, 105, 106, 111, 119, 123, 125, 126, 108], [0, 3, 5, 6, 12, 17, 18, 20, 24, 33, 34, 39, 40, 43, 45, 46, 51, 53, 54, 57, 58, 60, 65, 66, 71, 72, 75, 77, 78, 83, 85, 86, 89, 90, 92, 99, 105, 106, 111, 119, 125, 126, 123]]
def show_Q7():
    seq = [114, 74, 117, 16, 127, 76, 96, 29, 19, 47, 124, 88, 7, 22, 102, 9, 2, 94, 79, 109, 69, 35, 14, 56, 87, 44, 59, 91, 4, 121, 84, 52, 67, 26, 107, 49, 42, 62, 81, 37, 55, 103, 73, 28, 50, 32, 70, 1, 21, 64, 115, 110, 82, 41, 104, 61, 27, 38, 93, 15, 8, 122, 116, 97, 112, 31, 95, 48, 120, 113, 80, 30, 63, 23, 100, 118, 98, 108, 11, 13, 25, 101, 68, 36, 10, 123, 89, 39, 125, 6, 20, 66, 12, 58, 75, 65, 99, 71, 119, 85, 43, 45, 83, 105, 92, 34, 51, 46, 78, 5, 53, 111, 18, 106, 60, 17, 54, 57, 3, 72, 33, 86, 24, 40, 77, 126, 90, 0]
    hilite=Q7_bigbags[0]
    common_Qn_show(7,seq, hilite, bbox=ig.BoundingBox(1024,500))

def show_Q6_TD():
    seq=[55, 62, 21, 35, 28, 31, 61, 0, 44, 41, 37, 18, 52, 14, 11, 13, 7, 38, 25, 47, 49, 56, 42, 59, 8, 22, 19, 4, 63, 32, 26, 50,
 1, 60, 16, 2, 6, 45, 34, 10, 3, 54, 40, 15, 39, 57, 43, 9, 12, 24, 27, 20, 36, 48, 5, 23, 30, 58, 29, 33, 46, 17, 53, 51] 
    hilite=[3, 5, 9, 10, 12, 15, 17, 20, 23, 24, 27, 30, 33, 34, 36, 39, 40, 46, 48, 51, 54, 58, 6]

    hilite2= [5, 9, 12, 15, 17, 20, 23, 24, 27, 29, 30, 33, 36, 39, 40, 43, 46, 48, 51, 53, 57, 58, 54]
    common_Qn_show(6,seq, hilite, hilite2, bbox=ig.BoundingBox(800,400))

def show_Q5_TD():
    seq = [0, 3, 5, 9, 17, 6, 10, 18, 12, 20, 24, 15, 23, 27, 29, 30, 1, 2, 4, 8, 16, 7, 11, 19, 13, 21, 14, 22, 25, 26, 28, 
31]
    hilite =[4, 7, 8, 11, 13, 14, 16, 19, 21, 22, 25, 26, 28]
    hilite2 = [20, 12, 24]
    return common_Qn_show(5,seq,hilite,hilite2)

def show_Q4_TD():

    seq=[9, 1, 3, 5, 11, 7, 15, 13, 12, 4, 14, 2, 8, 6, 16, 10]
    seq.reverse()
    #seq=[1, 4, 6, 10, 7, 11, 13, 16, 2, 3, 5, 9, 8, 12, 14, 15]  #Q4
    seq=[i-1 for i in seq]
    hilite = [2,4,7,8,11,13,14,   6,10,12,15]
    hilite2=[6,10,12,15]
    
    common_Qn_show(4,seq,hilite,hilite2)

def Qn_layered_init(n):
    ret = []
    for i in range(0, 2**n):
        if bitcount(i) % 2:
            ret.append(i)
    return ret

def random_mindegree_TW(G_or_adjlist,force_init=[]):
    width = -1
    adj = [set(a) for a in G_or_adjlist.get_adjlist()]
    seq = []
    #H = EGraph(len(adj))
    for vnum in range(len(adj)):
        if force_init:
            v = force_init.pop(0)
        else:
            mindeg = min(len(adji) for i,adji in enumerate(adj) if adji is not None)
            options = [i for i,adji in enumerate(adj) if adji is not None and len(adji) == mindeg]
            v = random.choice(options)
        seq.append(v)
        higher_n = [i for i in adj[v] if adj[i] is not None]
        #H += [(v,j) for j in adj[v]]  
        #for u in adj[v]:   #faster? try it
        #    u.remove(v)
        adj[v] = None
        for u in adj:
            if u is not None and v in u:
                u.remove(v)
        width = max(width, len(higher_n))
        for v1,v2 in combinations(higher_n, 2):
            adj[v1].add(v2)
            adj[v2].add(v1)
            #H += (v1,v2)
    #print seq
    #if not is_perfect_elim_ordering(H, seq):
    #    print "ERROR!===============Not a perfect elim ordering"
    return seq, width

def random_minfill_TW(G_or_adjlist,force_init=[]):
    width = -1
    adj = [set(a) for a in G_or_adjlist.get_adjlist()]
    seq = []
    fillin = [len([i for i in adj[v] if adj[i] is not None]) for v in range(len(adj))]
    #H = EGraph(len(adj))
    for vnum in range(len(adj)):
        if force_init:
            v = force_init.pop(0)
        else:
            mindeg = min(len(adji) for i,adji in enumerate(adj) if adji is not None)
            options = [i for i,adji in enumerate(adj) if adji is not None and len(adji) == mindeg]
            v = random.choice(options)
        seq.append(v)
        higher_n = [i for i in adj[v] if adj[i] is not None]
        #H += [(v,j) for j in adj[v]]  
        updateset = set()
        for u in adj[v]:
            u.remove(v)
            updateset.update(u)
        adj[v] = None
        width = max(width, len(higher_n))
        for v1,v2 in combinations(higher_n, 2):
            adj[v1].add(v2)
            adj[v2].add(v1)
            #H += (v1,v2)
    #print seq
    #if not is_perfect_elim_ordering(H, seq):
    #    print "ERROR!===============Not a perfect elim ordering"
    return seq, width
    

def mindegree_test(G):
    for i in range(1000):
        s,w = random_mindegree_TW(G)
        TD,H = TD_from_seq(G,s)
        if w != TD.width():
            print "mismatch", w, TD.width()
            return


def random_seq_width_histogram(G, num=1000):
    ret = ig.Histogram(1)
    start_t = time.clock()
    seq = range(G.vcount())
    adj = [set(a) for a in G.get_adjlist()]
    for i in xrange(num):
        random.shuffle(seq)
        #ans1 = TW_from_seq(G,seq)
        #ans2 = TD_from_seq(G,seq)[0].width()
        ans1 = fast_TW_from_seq(G,seq)
        #ans2 = sorted_TW(G,seq)[0].width()
        #if ans1 != ans2:
        #    print "error", ans1, ans2, seq
        #    return
        ret.add(ans1)
    print "random_seq_width_histogram:", num, "seqs evaluated in", time.clock() - start_t, "s"
    return ret

def random_mindeg_width_histogram(G, num=1000, force_init=[]):
    ret = ig.Histogram(1)
    start_t = time.clock()
    best = [[], G.vcount()]
    for i in xrange(num):
        seq,ans1 = random_mindegree_TW(G, force_init)
        if ans1 < best[1]:
            best = seq, ans1
        ret.add(ans1)
    print "random_mindeg_width_histogram:", num, "seqs evaluated in", time.clock() - start_t, "s"
    print "best sequence (width %d):" % best[1]
    print str(best[0])
    return ret, best[0]


# test

# g = ig.Graph(5)
# #g.vs['label'] = ['1'] * g.vcount()
# g += [(0,1), (1,2), (2,3), (0,3), (1,4)]
# h = PreTreeDecomp.from_graph(g)

G = EGraph.Random(11,0.3)
#N = EGraph(1)
#N.vs[0]['bag'] = range(G.vcount())

#seq = range(G.vcount())
#random.shuffle(seq)
seq,width = random_mindegree_TW(G)
TD, H = TD_from_seq(G, seq)
TD.reduce_TD()
def test1():
    G.plot()
    TD.plot()
    TD.reduce_TD()
    TD.plot()

if __name__ == '__main__':
    mindegree_test()
    print "Tests passed"
